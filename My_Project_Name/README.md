# MY_EXPERIMENT_NAME
Author: FIRST_NAME LAST_NAME (_EMAIL_)

Year: YEAR_OF_CREATION

Version: VERSION_NUMBER

## DESCRIPTION
In this short section, you should describe the purpose of this project.

## METHOD
In this section, you must describe the equipment this project is using as well as
the general procedure.

### APPARATUS
Here goes the list of the equipment used in your project. For example:
* Eye-tracker: SR Research Eyelink 1000, 35mm binocular lens

### PROCEDURE
Short description of the procedure.

## MANUAL
The purpose of this section is to explain how to run your experiment and data
analyses in the "Experiment" and "Analysis" sub-sections respectively.

### Experiment
Description of how to run your experiment.

### Data analysis
Description of data organization, how to use your analysis scripts, etc.
If no data are included in the "analyses" folder, please provide the reader with
a link to the location of your data.
