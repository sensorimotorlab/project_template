# Template description

## Summary
1. [Introduction](#introduction)
2. [Folder structure](#folder-structure)
3. [Source code documentation](#Source-code-documentation)

## Introduction
The purpose of this template is to provide your successors with a detailed
documentation about your projects so that they can easily replicate your findings
by either running your experiments again or by analyzing your data. In this folder,
they should be able to find the source code of your experiment, the data (or at least
a link to their storage location) and your analysis scripts.

The code of either your experiment or your analyses should be sufficiently documented so that anyone can understand what it does and how to use it. You may find further instructions in the "source code documentation" section.

**Part of this information could have been organized through the Research Data Management System (RDM) of the Donders Institute. You do not have to double the information, but make clear the relation between this folder and the RDM folder.**

If you are about to leave the lab, then you should make an appointment with either Pieter or Luc to inform them about this information.

## Folder structure:
```
../My_Project_Name (This should be the name of your project)

  analyses/
      This folder must include your analysis scripts and functions.

      vendors/
          If your code uses dependencies (external functions, modules, etc.,
          not natively implemented in the programming language you use), then
          you can either include them in this sub-folder or give the list
          of the dependencies you are using and a link to where we can find them).

  data/
      This folder includes your data files.

  docs/
      This folder includes a version of your article (if the study has been
        published), or a version of your manuscript (rejected or not submitted)
        and any other documents (figures, movies).

  experiment/
      This folder includes the complete source code of your experiments.

      vendors/
        If your code uses dependencies (external functions, modules, etc.,
        not natively implemented in the programming language you use), then
        you can either include them in this sub-folder or give the list
        of the dependencies you are using and a link to where we can find them).

  README.md
      The complete description goes in this file and should respect the provided
      structure.

```

## Source code documentation
This section provides some examples of best practices in source code documentation.
Lines starting by "#" are commented line, so you may want to use the proper syntax
based on the programing language you use.

### Header
Every file should include a header providing some useful information about:
* the project it belongs to,
* the copyright license (if any) that defines the terms of usage, modification
and publication (every license provides a text to copy/paste in your file header)
* the author(s): name, email address

Here is an example:

```
# This file belongs to YOUR_PROJECT_NAME
#

!! BEGIN OF COPYRIGHT SECTION (The Copyright section is optional)
# Copyright (C) YEAR FULL_NAME, INSTITUTION
#
# Here should go the Copyright disclaimer, for example:
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
!! END OF COPYRIGHT SECTION

# This file includes:
# - List of your functions, classes

# author = "Your full name"
# email = "Your email" (preferably, provide an email address you will be able to
          access in the future)
# created = Year
# license = license name (e.g. "GPL"): This is optional
```

### Function and class documentation
#### Functions and objects methods
The documentation of a function or method should start with a short description
of its purpose.

Then follows the list of inputs:
  @param: variable_type variable_name - description of the variable
* Note that describing the variable_type (int, float, string, bool, etc.) is
obviously less critical in languages that explicitly require the declaration of
the variable type (e.g. C/C++)

Then the list of returned variables:

  @return: variable_type - description of the variable

  or, if the function does not return anything:

  @return: void

```
# This function does some useful stuff and eventually saves the world
# @param: string question - the question asked by the user
# @param: int age - user's age
# @return: string - eventually the answer to the question asked by the user
function my_function_name(question, age)

    if age > 99999
        return "Your are old enough to understand that the answer to the question
        of everything is 42."
      else
        return "The answer to this question you should get much later."

```

#### Classes
The documentation of classes is similar to that of functions with the exception
that it does not include the list of arguments.
If the class requires some external sources (files, other functions, ...), then
these sources should be listed as follows:

@required file_name

```
# This class will hold useful functions that together can definitely save
# the world
#
# Here goes a more detailed description of the purpose of this class.
#
# @required parameters.json
# @required data.csv
#
class MyClass {

    do_something
}
```
